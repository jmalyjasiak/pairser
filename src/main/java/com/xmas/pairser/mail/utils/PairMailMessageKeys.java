package com.xmas.pairser.mail.utils;

import lombok.AccessLevel;
import lombok.NoArgsConstructor;

@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class PairMailMessageKeys {

     public static final String TITLE = "mail.pair.title";
     public static final String MAIN_MESSAGE_TEMPLATE = "mail.pair.mainMessageTemplate";

     public static final String MAIN_MESSAGE_VARIABLE = "mainMessage";
     public static final String PAIR_VARIABLE = "pair";


     public static final String TEMPLATE_NAME = "PairEmail";
}

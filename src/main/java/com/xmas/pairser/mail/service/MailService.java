package com.xmas.pairser.mail.service;

import com.xmas.pairser.domain.pairser.model.dto.Group;
import com.xmas.pairser.domain.pairser.model.dto.Pair;
import com.xmas.pairser.mail.exception.MailSendingException;
import com.xmas.pairser.mail.factory.PairingFactory;
import io.vavr.CheckedConsumer;
import io.vavr.control.Try;
import lombok.AllArgsConstructor;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.stereotype.Service;

import javax.mail.internet.MimeMessage;

@Service
@AllArgsConstructor
public class MailService {

     private final JavaMailSender javaMailSender;
     private final PairingFactory pairingFactory;

     public void sendPairingEmails(Group group) {
          for (Pair pair : group.getPairs()) {
               new Thread(() -> Try.of(() -> pairingFactory.createMessage(pair, group))
                            .andThenTry((CheckedConsumer<? super MimeMessage>) javaMailSender::send)
                            .getOrElseThrow(MailSendingException::ofThrowable))
                       .start();
          }
     }
}

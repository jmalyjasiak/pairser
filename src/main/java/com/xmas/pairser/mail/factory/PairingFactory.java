package com.xmas.pairser.mail.factory;

import com.xmas.pairser.domain.pairser.model.dto.Group;
import com.xmas.pairser.domain.pairser.model.dto.Pair;
import com.xmas.pairser.mail.config.properties.MailExtendedProperties;
import com.xmas.pairser.mail.utils.LocalizedMessageSource;
import com.xmas.pairser.mail.utils.MultipartMimeMessageBuilder;
import lombok.RequiredArgsConstructor;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.MessageSource;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.stereotype.Component;
import org.thymeleaf.TemplateEngine;
import org.thymeleaf.context.Context;

import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;
import java.util.Locale;
import java.util.Map;

import static com.xmas.pairser.mail.utils.PairMailMessageKeys.*;
import static java.util.Map.entry;
import static java.util.Map.ofEntries;

@Component
@RequiredArgsConstructor
@EnableConfigurationProperties( { MailExtendedProperties.class })
public class PairingFactory {

     private final MailExtendedProperties mailExtendedProperties;
     private final TemplateEngine templateEngine;
     private final MessageSource messageSource;
     private final JavaMailSender javaMailSender;

     public MimeMessage createMessage(Pair pair, Group group) throws MessagingException {
          final LocalizedMessageSource localizedMessageSource =
                  new LocalizedMessageSource(messageSource, Locale.ROOT);

          return MultipartMimeMessageBuilder.of(javaMailSender)
                                            .from(mailExtendedProperties.getNoReplyAddress())
                                            .to(pair.getFrom())
                                            .subject(localizedMessageSource.get(TITLE))
                                            .htmlText(createHTMLContent(localizedMessageSource, pair, group))
                                            .getMessage();
     }

     private String createHTMLContent(LocalizedMessageSource messages, Pair pair, Group group) {
          final Map<String, Object> variables = buildVariables(messages, pair, group);
          Context context = new Context(messages.getLocale());
          context.setVariables(variables);
          return templateEngine.process(TEMPLATE_NAME, context);
     }

     private Map<String, Object> buildVariables(LocalizedMessageSource messages, Pair pair, Group group) {
          return ofEntries(
                  entry(PAIR_VARIABLE, pair.getTo()),
                  entry(MAIN_MESSAGE_VARIABLE, createMessageBody(messages, group.getName()))
          );
     }

     private String createMessageBody(LocalizedMessageSource messages, String username) {
          return String.format(messages.get(MAIN_MESSAGE_TEMPLATE), username);
     }
}
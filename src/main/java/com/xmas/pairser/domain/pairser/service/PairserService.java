package com.xmas.pairser.domain.pairser.service;

import com.xmas.pairser.domain.pairser.model.dto.GeneratePairsDTO;
import com.xmas.pairser.domain.pairser.model.dto.Group;
import com.xmas.pairser.domain.pairser.model.dto.Pair;
import com.xmas.pairser.mail.service.MailService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.Collections;
import java.util.LinkedList;
import java.util.List;
import java.util.Random;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

@Service
@RequiredArgsConstructor
public class PairserService {

    private final Random random = new Random();
    private final MailService mailService;

    public void generatePair(GeneratePairsDTO generatePairs) {
        final Set<String> receivers = generatePairs.getEmails();

        final Set<Pair> pairs = shuffleAndCreatePairs(new LinkedList<>(receivers));

        final Group group = Group.builder().name(generatePairs.getName()).pairs(pairs).build();

        mailService.sendPairingEmails(group);
    }

    private Set<Pair> shuffleAndCreatePairs(List<String> receivers) {
        final List<String> buyers = getShuffled(receivers);
        return IntStream.range(0, receivers.size())
                .boxed()
                .map(index -> generatePair(receivers.get(index), buyers.get(index)))
                .collect(Collectors.toSet());
    }

    private List<String> getShuffled(List<String> receivers) {
        final List<String> buyers = new LinkedList<>();
        final List<String> receiversCopy = new LinkedList<>(receivers);
        Collections.shuffle(receiversCopy);
        int index = 0;
        while(!receiversCopy.isEmpty()) {
            index = pickUser(receivers, buyers, receiversCopy, index);
        }
        return buyers;
    }

    private int pickUser(List<String> receivers, List<String> buyers, List<String> receiversCopy, int index) {
        int randIndex = random.nextInt(receiversCopy.size());
        final String randUser = receiversCopy.get(randIndex);
        if (!randUser.equals(receivers.get(index))) {
            rewriteAndDeleteUser(buyers, receiversCopy, randUser);
            index++;
        }
        return index;
    }

    private void rewriteAndDeleteUser(List<String> buyers, List<String> receiversCopy, String randUser) {
        buyers.add(randUser);
        receiversCopy.remove(randUser);
    }

    private Pair generatePair(String receiver, String buyer) {
        return Pair.builder().from(buyer).to(receiver).build();
    }
}

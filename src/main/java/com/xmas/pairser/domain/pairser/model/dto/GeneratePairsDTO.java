package com.xmas.pairser.domain.pairser.model.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotEmpty;
import java.util.Set;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class GeneratePairsDTO {

    @NotBlank
    private String name;

    @NotEmpty
    private Set<String> emails;
}

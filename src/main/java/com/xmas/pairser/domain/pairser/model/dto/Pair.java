package com.xmas.pairser.domain.pairser.model.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;


@Builder
@Data
@NoArgsConstructor
@AllArgsConstructor
public class Pair {

    private Long id;
    private String from;
    private String to;
}

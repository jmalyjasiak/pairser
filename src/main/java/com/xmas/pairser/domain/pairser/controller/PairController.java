package com.xmas.pairser.domain.pairser.controller;

import com.xmas.pairser.domain.pairser.model.dto.GeneratePairsDTO;
import com.xmas.pairser.domain.pairser.service.PairserService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;

@RestController
@RequestMapping("/pairs")
@RequiredArgsConstructor
public class PairController {

    private final PairserService pairserService;

    @PostMapping
    public void generatePairs(@RequestBody @Valid GeneratePairsDTO generatePairsDTO) {
        pairserService.generatePair(generatePairsDTO);
    }
}

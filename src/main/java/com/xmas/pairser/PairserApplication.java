package com.xmas.pairser;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class PairserApplication {

     public static void main(String[] args) {
          SpringApplication.run(PairserApplication.class, args);
     }
}
